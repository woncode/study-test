import com.woncode.configuration.HibernateConfig;
import com.woncode.configuration.WebMvcConfig;
import com.woncode.entity.TeacherEntity;
import com.woncode.service.TeacherService;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Wang Guiwen on 2017/7/9.
 */

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, HibernateConfig.class})
public class SpringTest {

    @Autowired
    private TeacherService service;

    private Logger logger = Logger.getLogger(SpringTest.class);

    @Before
    public void beforeTest(){
        System.out.println("begin test");
    }

    @Test
    @Rollback(false)
    public void dbStoreTest() {
        TeacherEntity entity =new TeacherEntity();
        entity.setName("t1");
        //entity = service.save(entity);
        System.out.println(service.count());
    }

    @Test
    public void condbTest(){
        try {
            Class.forName("org.postgresql.Driver");
            Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/study_test", "study_user", "123456");
            System.out.println("Opened database successfully");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void logTest(){
        logger.debug("log output");
    }

}
