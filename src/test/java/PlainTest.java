import com.woncode.Util.InitMethodExecutor;
import com.woncode.Util.RoleTypeEnum;
import org.junit.Test;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
public class PlainTest {

    @Test
    public void enumTest(){
        for (RoleTypeEnum roleType: RoleTypeEnum.values()){
            System.out.println(roleType.toString());
        }
    }

    @Test
    public void getAllClass() throws Exception {
        InitMethodExecutor initMethodExecutor = new InitMethodExecutor();
        initMethodExecutor.executeAllInitMethodByPackageName("com.woncode.service");
    }
}
