package com.woncode.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Created by Wang Guiwen on 2017/6/29.
 */
public class WebAppInitConfig
        extends AbstractAnnotationConfigDispatcherServletInitializer
        // implements WebApplicationInitializer
{

    // @Override
    // public void onStartup(ServletContext container) throws ServletException {
    //    // Create the 'root' Spring application context
    //    AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
    //    rootContext.register(RootConfig.class);//这是自定义的配置类，这个配置类会影响项目全局
    //
    //    // Manage the lifecycle of the root application context
    //    container.addListener(new ContextLoaderListener(rootContext));
    //
    //    // Create the dispatcher servlet's Spring application context
    //    AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
    //    dispatcherContext.register(WebMvcConfig.class);//这是自定义的配置类，这个配置类只会影响当前模块
    //
    //    // Register and map the dispatcher servlet
    //    ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher", new DispatcherServlet(dispatcherContext));
    //    dispatcher.setLoadOnStartup(1);
    //    dispatcher.addMapping("/");
    // }


    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class, SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebMvcConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
