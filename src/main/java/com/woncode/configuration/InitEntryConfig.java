package com.woncode.configuration;

import com.woncode.Util.InitMethodExecutor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
public class InitEntryConfig implements BeanPostProcessor {


    public InitEntryConfig(){
        super();
    }

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        InitMethodExecutor initMethodExecutor = new InitMethodExecutor();
        try {
            initMethodExecutor.executeAllInitMethodByPackageName("com.woncode.service");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }
}
