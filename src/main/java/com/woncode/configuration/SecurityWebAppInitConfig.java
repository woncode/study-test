package com.woncode.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by Wang Guiwen on 2017/7/16.
 */
public class SecurityWebAppInitConfig extends AbstractSecurityWebApplicationInitializer {
}
