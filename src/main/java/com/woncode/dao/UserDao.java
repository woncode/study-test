package com.woncode.dao;

import com.woncode.entity.UserEntity;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
public interface UserDao extends BaseJpaDao<UserEntity, Long> {

    UserEntity findByName(String name);
}
