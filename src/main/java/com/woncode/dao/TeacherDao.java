package com.woncode.dao;

import com.woncode.entity.TeacherEntity;

/**
 * Created by Wang Guiwen on 2017/7/15.
 */
public interface TeacherDao extends BaseJpaDao<TeacherEntity, Long> {
}
