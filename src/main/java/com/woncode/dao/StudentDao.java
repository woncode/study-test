package com.woncode.dao;

import com.woncode.entity.StudentEntity;

/**
 * Created by Wang Guiwen on 2017/7/15.
 */
public interface StudentDao extends BaseJpaDao<StudentEntity, Long> {
}
