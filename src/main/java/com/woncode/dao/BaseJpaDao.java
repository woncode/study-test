package com.woncode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */

@NoRepositoryBean
public interface BaseJpaDao<EntityType, IdType extends Serializable> extends JpaRepository<EntityType, IdType> {
    @Override
    EntityType findOne(IdType id);

    @Override
    <S extends EntityType> S save(S s);

    @Override
    void delete(IdType id);

    @Override
    long count();
}
