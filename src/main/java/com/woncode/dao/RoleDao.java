package com.woncode.dao;

import com.woncode.entity.RoleEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */

@Repository
public interface RoleDao extends BaseJpaDao<RoleEntity, Long> {

    RoleEntity findByName(String name);
}
