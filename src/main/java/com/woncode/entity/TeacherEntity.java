package com.woncode.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Wang Guiwen on 2017/7/9.
 */

@Entity
@Table(name = "st_teacher")
public class TeacherEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "ref_teacher_student",
        joinColumns = @JoinColumn(name = "teacher_id",referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "student_id",referencedColumnName = "id")
    )
    private Set<StudentEntity> students = new HashSet<StudentEntity>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<StudentEntity> getStudents() {
        return students;
    }

    public void setStudents(Set<StudentEntity> students) {
        this.students = students;
    }
}
