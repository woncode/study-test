package com.woncode.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Wang Guiwen on 2017/7/9.
 */

@Entity
@Table(name = "st_student")
public class StudentEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @ManyToMany(mappedBy ="students")
    private Set<TeacherEntity> teachers = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TeacherEntity> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<TeacherEntity> teachers) {
        this.teachers = teachers;
    }
}
