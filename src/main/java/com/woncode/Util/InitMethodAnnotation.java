package com.woncode.Util;

import java.lang.annotation.*;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface InitMethodAnnotation {
    long order() default 0;
}
