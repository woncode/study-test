package com.woncode.Util;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
public enum RoleTypeEnum {
    Admin, Teacher, Student;
}
