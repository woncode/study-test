package com.woncode.service;

import com.woncode.dao.TeacherDao;
import com.woncode.entity.TeacherEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Wang Guiwen on 2017/7/15.
 */

@Service
public class TeacherService extends BaseService<TeacherEntity, TeacherDao>
{
}
