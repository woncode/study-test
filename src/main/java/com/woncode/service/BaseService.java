package com.woncode.service;

import com.woncode.dao.BaseJpaDao;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */

//public class BaseService<T, ID extends Serializable> {
public class BaseService<EntityType, JpaDaoType extends BaseJpaDao> {

    //@Autowired
    //BaseJpaDao<T, ID> jpaDao;

    @Autowired
    JpaDaoType jpaDao;

    public EntityType save(EntityType entity) {
        return (EntityType) jpaDao.save(entity);
    }

    public EntityType findOne(Long id) {
        return (EntityType) jpaDao.findOne(id);
    }

    public void delete(Long id){
        jpaDao.delete(id);
    }

    public long count(){
        return jpaDao.count();
    }

}
