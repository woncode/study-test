package com.woncode.service;

import com.woncode.Util.InitMethodAnnotation;
import com.woncode.Util.RoleTypeEnum;
import com.woncode.dao.RoleDao;
import com.woncode.dao.UserDao;
import com.woncode.entity.RoleEntity;
import com.woncode.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
@Service
public class UserService extends BaseService<UserEntity, UserDao>
{

    @Autowired
    RoleDao roleDao;

    @InitMethodAnnotation(order = 2)
    public void init(){
        if (this.count() == 0){
            UserEntity userEntity = new UserEntity();
            userEntity.setName("admin");
            userEntity.setPassword("123");
            Set<RoleEntity> roleSet = new HashSet<>();
            roleSet.add(roleDao.findByName(RoleTypeEnum.Admin.toString()));
            userEntity.setRoles(roleSet);
            this.save(userEntity);
        }
    }

    public UserEntity findByName(String name) {
        return this.findByName(name);
    }
}
