package com.woncode.service;

import com.woncode.Util.InitMethodAnnotation;
import com.woncode.Util.RoleTypeEnum;
import com.woncode.dao.RoleDao;
import com.woncode.entity.RoleEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Wang Guiwen on 2017/9/15.
 */
@Service
public class RoleService extends BaseService<RoleEntity, RoleDao>
{

    @InitMethodAnnotation(order = 1)
    public void init(){
        if (this.count() == 0){
            for (RoleTypeEnum roleType: RoleTypeEnum.values()){
                RoleEntity roleEntity = new RoleEntity();
                roleEntity.setName(roleType.toString());
                this.save(roleEntity);
            }
        }
    }

    public RoleEntity findByName(String name){
        return jpaDao.findByName(name);
    }
}
