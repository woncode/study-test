package com.woncode.service;

import com.woncode.dao.StudentDao;
import com.woncode.entity.StudentEntity;
import org.springframework.stereotype.Service;

/**
 * Created by Wang Guiwen on 2017/7/15.
 */

@Service
public class StudentService extends BaseService<StudentEntity, StudentDao>
{
}
