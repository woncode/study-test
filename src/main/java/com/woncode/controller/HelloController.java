package com.woncode.controller;

import com.woncode.entity.TeacherEntity;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Wang Guiwen on 2017/6/28.
 */

@Controller
public class HelloController{

    //@Autowired
    //private TeacherService teacherService;
    //@Autowired
    //private StudentService studentService;

    private Logger logger = Logger.getLogger(getClass());
    
    @RequestMapping("/")
    String home() {
        logger.debug("hello!");
        return "hello";
    }

    @ResponseBody
    @RequestMapping("/save")
    String save(TeacherEntity entity){
        return "ok";
    }
}
